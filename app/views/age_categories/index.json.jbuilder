json.array!(@age_categories) do |age_category|
  json.extract! age_category, :id, :name
  json.url age_category_url(age_category, format: :json)
end
