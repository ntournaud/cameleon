var currentQuestion=0;
  var totalCorrect=0;
  var totalWrong=0;
  var response ;//= {};
  var feed = {entry:0};
  //response.feed = feed;
  function getReply(data) { //Get data from Google Spreadsheet in JSON
    response=data;
  }

var isAnswered=false;
  $(document).ready(function(){
    // $("#flipQuestion").click(function(){
    // $("#questionPanel").slideToggle("medium");
    // if(response.feed.entry.length!=currentQuestion){
    //   var t=setTimeout(slideDown,200);
    // }
    // t=setTimeout(loadNewQuestion,250);
    // });
    r = $('#dragpadd');
    r.rangeinput();
  });
  function slideDown(){
    $("#questionPanel").slideToggle("medium");
  }
  function beginQuiz(startButton){
    startButton.style.display="none";
    document.getElementById("questionPanel").style.display="block";
    document.getElementById("outerQuizContainer").style.height="380px";
    loadNewQuestion();
  }
  function resetAllFields(){
    document.getElementById("wrongMessage").style.display="none";
    document.getElementById("correctMessage").style.display="none";
    document.getElementById("optiona").className="unselected";
    document.getElementById("optionb").className="unselected";
    if ("optionc") {
    document.getElementById("optionc").className="unselected";
    document.getElementById("optiond").className="unselected";
  }
    document.getElementById("optiona").style.display="block";
    document.getElementById("optionb").style.display="block";
    document.getElementById("optionc").style.display="block";
    document.getElementById("optiond").style.display="block";
    isAnswered=false;   
  }
  function loadNewQuestion(){
    resetAllFields();
    console.log(response);
    if(response.feed.entry.length==currentQuestion){
      document.getElementById("questionPanel").style.display="none";
      document.getElementById("totalScore").style.display="block";
      document.getElementById("flipQuestion").style.display="none";
      document.getElementById("totalScore").innerHTML= "Score Total "+totalCorrect+" sur "+response.feed.entry.length;
    }
    else{
      document.getElementById("questionTitle").innerHTML=response.feed.entry[currentQuestion].gsx$question.$t;
      if(response.feed.entry[currentQuestion].gsx$optiona.$t!=""){
        document.getElementById("optiona").innerHTML=response.feed.entry[currentQuestion].gsx$optiona.$t;
      }else{document.getElementById("optiona").style.display="none";}
      
      if(response.feed.entry[currentQuestion].gsx$optionb.$t!=""){
        document.getElementById("optionb").innerHTML=response.feed.entry[currentQuestion].gsx$optionb.$t;       
      }else{document.getElementById("optionb").style.display="none";}
      
      if(response.feed.entry[currentQuestion].gsx$optionc.$t!=""){
        document.getElementById("optionc").innerHTML=response.feed.entry[currentQuestion].gsx$optionc.$t;
      }else{document.getElementById("optionc").style.display="none";}
      
      if(response.feed.entry[currentQuestion].gsx$optiond.$t!=""){
        document.getElementById("optiond").innerHTML=response.feed.entry[currentQuestion].gsx$optiond.$t; 
      }else{document.getElementById("optiond").style.display="none";}
      
      document.getElementById("proceeding").innerHTML="Question "+(currentQuestion+1)+"/"+response.feed.entry.length;
      document.getElementById("totalCorrectAnswers").innerHTML=totalCorrect+" bonne(s) réponse(s)";
      document.getElementById("flipQuestion").style.display="none";
      currentQuestion++;
    }
  }
  function selectAnswer(button,option){ //Invoked when user click on any answer options    
    if(!isAnswered){
        document.getElementById("flipQuestion").style.display="none";   
        button.className='normalButton';
        isAnswered=true;
            
        document.getElementById("optiona").className="afterSelect";
        document.getElementById("optionb").className="afterSelect";
        document.getElementById("optionc").className="afterSelect";
        document.getElementById("optiond").className="afterSelect";
        
        if(response.feed.entry[currentQuestion-1].gsx$answer.$t==option){
          totalCorrect++;
          button.className='correctAnswer';
          document.getElementById("correctMessage").style.display="block";
        } 
        else{
          totalWrong++;
          button.className='wrongAnswer';
          document.getElementById("wrongMessage").style.display="block";
        } 
        document.getElementById("option"+response.feed.entry[currentQuestion-1].gsx$answer.$t).className="correctAnswer";
        document.getElementById("totalCorrectAnswers").innerHTML=totalCorrect+" bonne(s) réponse(s)";

    } 
    setTimeout(function() {
    $("#questionPanel").slideToggle("medium");
        if(response.feed.entry.length!=currentQuestion){var t=setTimeout(slideDown,200);}
        t=setTimeout(loadNewQuestion,250);
    }, 1000);
  }

