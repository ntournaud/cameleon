#WORD-STORIES
class Dictionnary < ActiveRecord::Base
  validates :story_id, presence: true
  validates :word_id, presence: true

  belongs_to :story
  belongs_to :word
end
