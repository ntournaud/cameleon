class Story < ActiveRecord::Base
	has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }

  validates :description, presence: true
  validates :image, presence: true
  validates :key, presence: true

  belongs_to :book
  #USER-STORY
  has_many :libraries
  has_many :users, through: :libraries
  #WORD-STORIES
  has_many :dictionnaries
  has_many :words, through: :dictionnaries

  has_many :questions
  belongs_to :age_category


end
