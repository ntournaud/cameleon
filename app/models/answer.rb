class Answer < ActiveRecord::Base
  belongs_to :question
  has_many :panels
  has_many :users, through: :panels
end
