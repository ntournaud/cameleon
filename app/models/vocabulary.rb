#USER-WORD
class Vocabulary < ActiveRecord::Base
  validates :user_id, presence: true
  validates :word_id, presence: true

  belongs_to :user
  belongs_to :word
end
