class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :name, presence: true

  #USER-STORY
  has_many :libraries
  has_many :stories, through: :libraries
  
  #USER-WORDS
  has_many :vocabularies
  has_many :words, through: :vocabularies
  
  #USER-ANSWERS
  has_many :panels
  has_many :answers, through: :panels

  
end
