class AgeCategory < ActiveRecord::Base
  has_many :books
  has_many :stories
end
