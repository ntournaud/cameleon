class Book < ActiveRecord::Base
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  has_many :stories
  belongs_to :age_category

  validates :title, presence: true
  validates :image, presence: true
end
