class Question < ActiveRecord::Base
  belongs_to :story
  has_many :answers
end
