 class Word < ActiveRecord::Base
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }

  #USER-WORDS
  has_many :vocabularies
  has_many :users, through: :vocabularies
  
  #WORD-STORIES
  has_many :dictionnaries
  has_many :stories, through: :dictionnaries

end
