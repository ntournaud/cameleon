class Library < ActiveRecord::Base
  validates :story_id, presence: true
  validates :user_id, presence: true

  belongs_to :story
  belongs_to :user
end
