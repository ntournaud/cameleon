module BooksHelper
  def words_in_book(book)
    @S = 0
    book.stories.each do |story|
      @S += story.words.count
    end
    @S
  end
end
