ActiveAdmin.register Dictionnary do
  permit_params :id, :story_id, :word_id, :created_at, :updated_at
  
  index do
    column :id
    column "Histoire", :story
    column "Mot", :word
    actions
  end

end