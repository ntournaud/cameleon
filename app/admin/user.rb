ActiveAdmin.register User do
  permit_params :id, :name, :email, :sign_in_count, :current_sign_in_at, :last_sign_in_at, :created_at, :updated_at

  index do
    column :id
    column "Nom", :name
    column "Email", :email
    column "Nombre de connexions", :sign_in_count
    column "Dernière connexion", :last_sign_in_at
    actions
  end

end