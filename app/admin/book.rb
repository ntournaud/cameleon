ActiveAdmin.register Book do
  permit_params :id, :title, :created_at, :updated_at, :image_file_name, :image_content_type, :image_file_size, :image_updated_at, :image, :age_category_id

  index do
    column :id
    column "Titre", :title
    column "Catégorie", :age_category
    actions
  end

  form do |f|
    f.inputs "Rentrer un livre" do
      f.input :title
      f.input :image, as: :file, hint: f.template.image_tag(f.object.image.url(:thumb))
      f.input :age_category, as: :select, collection: AgeCategory.all.map {|u| [u.name, u.id]}
    end
    f.actions
  end

end