ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do

    section "Histoires récemment ajoutées" do
    table_for Story.order("created_at desc").limit(5) do
      column "Nom", :description
      column "Créée le", :created_at
    end
    strong { link_to "Voir toutes les histoires ajoutées", admin_stories_path }
    end
    # Here is an example of a simple dashboard with columns and panels.
    #
#     columns do
#       column do
#         panel "Recent Stories" do
#           ul do
#             Story.order("created_at desc").limit(5).each do |story|
#               li link_to(story.description, admin_story_path(story))
#             end
#           end
#         end
#       end
#           strong { link_to "Voir toutes les histoires ajoutées", admin_stories_path }
# end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content
end
