ActiveAdmin.register AgeCategory do
  permit_params :id, :name, :created_at, :updated_at

  index do
    column :id
    column "Catégorie", :name
    actions
  end

end