ActiveAdmin.register Story do
  permit_params :id, :description, :created_at, :updated_at, :image_file_name, 
                :image_content_type, :image_file_size, :image_updated_at, 
                :image, :content, :key, :book_id, :age_category_id

  index do
    column :id
    column "Titre", :description
    column "Livre", :book_id
    column "Clef du quizz", :key
    column "Age category", :age_category
    actions
  end

  form do |f|
    f.inputs "Rentrer un texte" do
      f.input :description
      f.input :image, as: :file, hint: f.template.image_tag(f.object.image.url(:thumb))
      f.input :content
      f.input :key
      f.input :book, as: :select, collection: Book.all.map {|u| [u.title, u.id]}
      f.input :age_category, as: :select, collection: AgeCategory.all.map {|u| [u.name, u.id]}
    end
    f.actions
  end
  
  controller do
    before_action :set_story, only: [:show, :edit, :update, :destroy]

    def create
      if @story = Story.create(story_params)
        populate_word_table
        instance_book(@story)
        redirect_to admin_story_path(@story), notice: 'Story was successfully created.'
      else
        render action: 'new'
      end
    end

    def update
      if @story.update(story_params)
        populate_word_table
        instance_book(@story)
        redirect_to admin_story_path(@story), notice: 'Story was successfully updated.'
      else
        render action: 'edit'
      end
    end

    protected

      def set_story
        @story = Story.find(params[:id])
      end

      def story_params
        params.require(:story).permit(:description, :image, :text, :content, :key, :book_id, :age_category_id)
      end

      def dico?(story, word)
        Dictionnary.find_by(story_id: story.id, word_id: word.id)
      end

      def word_registered?(word)
        Word.find_by(name: word)
      end

      def register_word(word)
        @story.words.create(name: word)
      end

      def populate_word_table
        @story.content.scan(/[^*!@%'’,\^\s\.]+/i).each do |word|
          word = word.downcase
          #si mot existe, vérifier s'il existe dans le dictionnaire
          if word_registered?(word)
            match_word = word_registered?(word)
            Dictionnary.create(story_id: @story.id, word_id: match_word.id) unless dico?(@story, match_word)
          else
            register_word(word)
          end
        end
      end

      def instance_book(story)
        if story.book
          story.age_category_id = story.book.age_category_id
        else
          book = Book.create(title: story.description, image_file_name: story.image_file_name, 
                    image_content_type: story.image_content_type,
                    image_file_size: story.image_file_size,
                    image_updated_at: story.image_updated_at,
                    age_category_id: story.age_category_id)
          story.book_id = book.id
        end
        story.save
      end
  end

end