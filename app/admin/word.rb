ActiveAdmin.register Word do
  permit_params :id, :name, :grammar, :definition, :created_at, :updated_at

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  index do
    column :id
    column "Mot", :name
    column "Type", :type
    column "Définition", :definition
    actions
  end

  form do |f|
    f.inputs "Rentrer un mot" do
      f.input :name
      f.input :grammar
      f.input :definition
    end
    f.actions
  end

end