ActiveAdmin.register Question do
  permit_params :story_id, :title, :created_at, :updated_at
  belongs_to :story

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  index do
    column :id
    column "Histoire", :story_id
    column "Question", :title
    actions
  end

end