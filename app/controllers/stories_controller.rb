  class StoriesController < ApplicationController
  before_action :set_story, only: [:show]
  before_action :paginate, only: [:index, :mystories]
  before_action :increase_word_counter, :increase_story_counter, only: [:show]
 
  private
    def set_story
      @story = Story.find(params[:id])
    end

    def paginate
      @stories = Story.all.order("created_at DESC").paginate(:page => params[:page], :per_page => 12)
    end

    def story_params
      params.require(:story).permit(:description, :image, :text, :content, :key, :book_id)
    end

    def increase_word_counter
      if current_user
        @story.words.each { |word| current_user.words << word unless current_user.words.find_by(name: word.name) }
      end
    end

    def increase_story_counter
      if current_user
        Library.create(user_id: current_user.id, story_id: @story.id) unless Library.find_by(user_id: current_user.id, story_id: @story.id)
      end
    end
end