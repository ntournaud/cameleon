class BooksController < InheritedResources::Base
  before_action :set_book, only: [:show]
  before_action :paginate, only: [:index, :main, :show]

  def index
    @books = Book.all.order("created_at DESC").paginate(:page => params[:page], :per_page => 12)
  end

  private
    def set_book
      @book = Book.find(params[:id])
    end

    def paginate
      @books = Book.all.order("created_at DESC").paginate(:page => params[:page], :per_page => 12)
    end
end
