class ApplicationController < ActionController::Base
  include ActionView::Helpers::DateHelper
  helper_method :words_today, :daily_activity, :count_stories, :count_read_words, :last_read, :last_read_story
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?
  protected

 def configure_permitted_parameters
   devise_parameter_sanitizer.for(:sign_up) << :name
   devise_parameter_sanitizer.for(:account_update) << :name
 end

    
  def words_today(user)
      user.vocabularies.where('vocabularies.created_at > ?', 1.days.ago)
  end

  def daily_activity(user)
    days = 0
    i = 0
    while user.vocabularies.where('vocabularies.created_at >= ? AND vocabularies.created_at <= ?', (i+1).days.ago, i.days.ago).length != 0
      days += 1
      i += 1
    end
    days
  end

  def count_stories(user)
    Library.find_by(user_id: user.id) ? Library.where(user_id: user.id).count : 0
  end

  def count_read_words(user)
    w = 0
    Library.where(user_id: user.id).each do |library|
      story = Story.find(library.story_id)
      w += story.content.scan(/[^*!@%'’,\^\s\.]+/i).count
    end
    w
  end

  def last_read(user)
    distance_of_time_in_words(Time.now, Library.where(user_id: user.id).order('created_at DESC').first.created_at) if Library.where(user_id: user.id).order('created_at DESC').first
  end

  def last_read_story(user, story)
    distance_of_time_in_words(Time.now, Library.where(user_id: user.id, story_id: story.id).first.created_at)
  end

end
