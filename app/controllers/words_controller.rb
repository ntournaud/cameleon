class WordsController < ApplicationController
  before_action :set_word, only: [:show, :edit, :update, :destroy]

  def index
    @words = Word.all
  end

  def show
  end

  def new
    @word = Word.new
  end

  def edit
  end

  def create
    @word = Word.new(word_params)
    if @word.save
      redirect_to @word, notice: 'Word was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    if @word.update(word_params)
      redirect_to @word, notice: 'Word was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @word.destroy
    redirect_to words_url
  end

  def get_def
    puts params[:word]
    @word = Word.find_by name:(params[:word])
    respond_to do |format|
      format.html { render :layout => false }
    end
  end

  private
    def set_word
      @word = Word.find(params[:id])
    end

    def word_params
      params.require(:word).permit(:name, :definition, :grammar)
    end
end
