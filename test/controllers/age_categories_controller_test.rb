require 'test_helper'

class AgeCategoriesControllerTest < ActionController::TestCase
  setup do
    @age_category = age_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:age_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create age_category" do
    assert_difference('AgeCategory.count') do
      post :create, age_category: { name: @age_category.name }
    end

    assert_redirected_to age_category_path(assigns(:age_category))
  end

  test "should show age_category" do
    get :show, id: @age_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @age_category
    assert_response :success
  end

  test "should update age_category" do
    patch :update, id: @age_category, age_category: { name: @age_category.name }
    assert_redirected_to age_category_path(assigns(:age_category))
  end

  test "should destroy age_category" do
    assert_difference('AgeCategory.count', -1) do
      delete :destroy, id: @age_category
    end

    assert_redirected_to age_categories_path
  end
end
