class FixDictionnaryColumnNameToStoryId < ActiveRecord::Migration
  def change
    rename_column :dictionnaries, :pin_id, :story_id
  end
end
