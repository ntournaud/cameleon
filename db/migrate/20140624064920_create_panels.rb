class CreatePanels < ActiveRecord::Migration
  def change
    create_table :panels do |t|
      t.string :user_id
      t.string :integer
      t.integer :answer_id
      t.datetime :created_at

      t.timestamps
    end
  end
end
