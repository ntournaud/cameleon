class AddColumnContentToPins < ActiveRecord::Migration
  def change
    add_column :pins, :content, :string
  end
end
