class AddAgeCategoryToStories < ActiveRecord::Migration
  def change
    add_column :stories, :age_category_id, :integer
  end
end
