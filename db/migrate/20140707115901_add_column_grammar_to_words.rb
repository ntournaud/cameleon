class AddColumnGrammarToWords < ActiveRecord::Migration
  def change
    add_column :words, :grammar, :string
  end
end
