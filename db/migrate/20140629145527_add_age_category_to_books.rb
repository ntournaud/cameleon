class AddAgeCategoryToBooks < ActiveRecord::Migration
  def change
    add_column :books, :age_category_id, :integer
  end
end
