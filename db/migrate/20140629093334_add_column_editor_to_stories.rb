class AddColumnEditorToStories < ActiveRecord::Migration
  def change
    add_column :stories, :editor, :string
  end
end
