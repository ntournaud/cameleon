class CreateVocabulary < ActiveRecord::Migration
  def change
    create_table :vocabularies do |t|
      t.integer :user_id
      t.integer :word_id
      t.datetime :created_at

      t.timestamps
    end
  end
end