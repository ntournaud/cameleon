class ChangeUserIdFormatInLibraries < ActiveRecord::Migration
  def change
    execute <<-SQL
    ALTER TABLE libraries
      ALTER COLUMN user_id TYPE integer USING (user_id::integer)
    SQL
  end
end
