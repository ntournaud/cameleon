class ChangeContentFormatInPins < ActiveRecord::Migration
  def up
   change_column :pins, :content, :text
  end

  def down
   change_column :pins, :content, :string
  end
end
