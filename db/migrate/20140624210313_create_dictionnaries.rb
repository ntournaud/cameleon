class CreateDictionnaries < ActiveRecord::Migration
  def change
    create_table :dictionnaries do |t|
      t.integer :word_id
      t.integer :pin_id

      t.timestamps
    end
  end
end
