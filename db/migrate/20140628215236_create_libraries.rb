class CreateLibraries < ActiveRecord::Migration
  def change
    create_table :libraries do |t|
      t.string :user_id
      t.string :integer
      t.integer :story_id

      t.timestamps
    end
  end
end
