class RemoveColumnIntegerFromPanels < ActiveRecord::Migration
  def change
    remove_column :panels, :integer, :string
  end
end
