class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.references :question, index: true
      t.string :title
      t.boolean :is_valid?

      t.timestamps
    end
  end
end
