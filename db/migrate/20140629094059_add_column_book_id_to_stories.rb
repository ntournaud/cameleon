class AddColumnBookIdToStories < ActiveRecord::Migration
  def change
    add_column :stories, :book_id, :integer
  end
end
