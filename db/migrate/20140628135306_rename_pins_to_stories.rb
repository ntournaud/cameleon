class RenamePinsToStories < ActiveRecord::Migration
  def change
    rename_table :pins, :stories
  end
end
