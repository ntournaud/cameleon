class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :pin, index: true
      t.string :title
      t.boolean :is_valid?

      t.timestamps
    end
  end
end
