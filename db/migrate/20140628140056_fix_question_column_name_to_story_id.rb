class FixQuestionColumnNameToStoryId < ActiveRecord::Migration
  def change
    rename_column :questions, :pin_id, :story_id
  end
end
